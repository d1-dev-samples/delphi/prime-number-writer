unit main;

interface

uses
  Windows, SysUtils, Forms, NumberWriter, ExtCtrls, StdCtrls, Controls, Classes;

type
  TForm1 = class(TForm)
    btStart: TButton;
    pnlLast: TPanel;
    pnlTime: TPanel;
    btClose: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;
    edLimit: TEdit;
    Label3: TLabel;
    procedure btStartClick(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);

  private
    nws, nw1, nw2: TNumberWriter;
    threadCounter: integer;
    timeStart: Cardinal;
    lastWrittenNumber: integer;

    procedure onFinish(Sender: TObject);
    procedure onProgress(num: integer);
    procedure updateGUI;
  end;

var
  Form1: TForm1;

implementation

uses
  PrimeNumberWriterThread;

{$R *.dfm}

procedure TForm1.btStartClick(Sender: TObject);
var
  t1: TPrimeNumberWriterThread;
  t2: TPrimeNumberWriterThread;
  limit: integer;
begin
  btStart.Enabled := false;
  timeStart := GetTickCount;
  limit := StrToIntDef(edLimit.Text, 0);

  nws := TNumberWriter.Create('result.txt');
  nw1 := TNumberWriter.Create('thread1.txt');
  nw2 := TNumberWriter.Create('thread2.txt');

  t1 := TPrimeNumberWriterThread.Create(nws, nw1, limit, onFinish, onProgress);
  t2 := TPrimeNumberWriterThread.Create(nws, nw2, limit, onFinish, onProgress);

  threadCounter := 2;
  t1.Resume;
  t2.Resume;

  lastWrittenNumber := 0;
  timer1.Enabled := true;

end;

// This method is synchronized
// We can update GUI from here
procedure TForm1.onFinish(Sender: TObject);
begin
  Dec(threadCounter);
  if threadCounter <= 0 then begin
    FreeAndNil(nws);
    FreeAndNil(nw1);
    FreeAndNil(nw2);
    timer1.Enabled := false;
    btStart.Enabled := true;
    updateGUI;
  end;
end;

// Warning! This method is not thread-safe!
// Do not update GUI from here!
procedure TForm1.onProgress(num: integer);
begin
  lastWrittenNumber := num;
end;


procedure TForm1.Timer1Timer(Sender: TObject);
begin
  updateGUI;
end;


procedure TForm1.updateGUI;
begin
  pnlLast.Caption := IntToStr(lastWrittenNumber);
  pnlTime.Caption := IntToStr(GetTickCount - timeStart);
end;


procedure TForm1.btCloseClick(Sender: TObject);
begin
  close;
end;

end.
