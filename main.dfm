object Form1: TForm1
  Left = 676
  Top = 298
  Caption = #1055#1080#1096#1077#1084' '#1092#1072#1081#1083#1099' '#1089' '#1087#1088#1086#1089#1090#1099#1084#1080' '#1095#1080#1089#1083#1072#1084#1080
  ClientHeight = 226
  ClientWidth = 407
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  DesignSize = (
    407
    226)
  TextHeight = 13
  object Label3: TLabel
    Left = 16
    Top = 111
    Width = 40
    Height = 16
    Caption = #1051#1080#1084#1080#1090
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
  end
  object btStart: TButton
    Left = 16
    Top = 169
    Width = 125
    Height = 46
    Anchors = [akLeft, akBottom]
    Caption = #1057#1090#1072#1088#1090
    Default = True
    TabOrder = 0
    OnClick = btStartClick
  end
  object pnlLast: TPanel
    Left = 16
    Top = 12
    Width = 377
    Height = 41
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 3
      Top = 2
      Width = 40
      Height = 16
      Caption = #1063#1080#1089#1083#1086
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
  end
  object pnlTime: TPanel
    Left = 16
    Top = 60
    Width = 377
    Height = 41
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Label2: TLabel
      Left = 3
      Top = 2
      Width = 72
      Height = 16
      Caption = #1042#1088#1077#1084#1103', '#1084#1089
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
  end
  object btClose: TButton
    Left = 268
    Top = 169
    Width = 125
    Height = 46
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 3
    OnClick = btCloseClick
  end
  object edLimit: TEdit
    Left = 16
    Top = 131
    Width = 122
    Height = 26
    Alignment = taCenter
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Text = '1000000'
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 360
    Top = 120
  end
end
