unit PrimeNumberWriterThread;

interface
uses
  Windows,
  NumberWriter,
  Classes;

type
  TProgressEvent = procedure(num: integer) of object;


  TPrimeNumberWriterThread = class(TThread)
  public
    Constructor Create(AWriterShared, AWriterOwn: TNumberWriter; ALimit: integer;
          AOnFinish: TNotifyEvent; AOnProgress: TProgressEvent); overload;

  protected
    procedure Execute; override;

  private
    writerShared: TNumberWriter;
    writerOwn: TNumberWriter;
    limit: integer;
    onFinish: TNotifyEvent;
    onProgress: TProgressEvent;

    procedure finish;

    function isPrime(n: integer): Boolean;
  end;

implementation

{ TPrimeNumberRangeWriter }

constructor TPrimeNumberWriterThread.Create(
      AWriterShared, AWriterOwn: TNumberWriter; ALimit: integer;
      AOnFinish: TNotifyEvent; AOnProgress: TProgressEvent);
begin
  inherited Create(true);
  writerShared := AWriterShared;
  writerOwn := AWriterOwn;
  limit := ALimit;
  onFinish := AOnFinish;
  onProgress := AOnProgress;
end;


procedure TPrimeNumberWriterThread.Execute;
var
  i: Integer;
begin
  i := 2;
  while( i <= limit ) do begin
    if isPrime(i) then begin

      // speed optimization (makes sense for slow machines)
      while( writerShared.isBusy ) do begin
        if( writerShared.getLastWrittenNumber >= i ) then
          Break;
      end;

      if (writerShared.getLastWrittenNumber >= i) then begin
        i := writerShared.getLastWrittenNumber;
      end else begin
        if writerShared.writeNumberSync(i) then begin
          writerOwn.writeNumber(i);

          if Assigned(onProgress) then
            onProgress( writerShared.getLastWrittenNumber );
        end;
      end;
    end;

    Inc(i);
  end;
  Synchronize(finish);
end;


procedure TPrimeNumberWriterThread.finish;
begin
  if Assigned(onFinish) then
    onFinish(self);
end;


function TPrimeNumberWriterThread.isPrime(n: integer): Boolean;
var
  i, q: integer;
begin
  result := false;

  if n <= 1 then
    exit;

  if n = 2 then begin
    Result := true;
    exit;
  end;

  if n mod 2 = 0 then
    exit;

  i := 3;
  q := Trunc(Sqrt(n));
  while i < q do begin

    // speed optimization (makes sense for slow machines)
    if (writerShared.getLastWrittenNumber >= n) then
      exit;

    if n mod i = 0 then
      exit;
    i := i + 2;
  end;

  result := true;
end;

end.
